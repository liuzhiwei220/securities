package com.securities.stock.dto;

import java.math.BigDecimal;

public class MaDto {
    /****
     * ma的指数名称
     */
    private String maName;
    /****
     * ma的指数的
     */
    private BigDecimal  maValue;

    public String getMaName() {
        return maName;
    }

    public void setMaName(String maName) {
        this.maName = maName;
    }

    public BigDecimal getMaValue() {
        return maValue;
    }

    public void setMaValue(BigDecimal maValue) {
        this.maValue = maValue;
    }

    public MaDto(String maName, BigDecimal maValue) {
        this.maName = maName;
        this.maValue = maValue;
    }
}
