package com.securities.stock.dto;

import com.securities.stock.pojo.Candle;

import java.math.BigDecimal;

public class CandleDto extends Candle {


    public CandleDto( String[]  columns) {
        setOpeningPrice(new BigDecimal(columns[1]));
        setYesterdayEndPrice(new BigDecimal(columns[2]));
        setCurrentPrice(new BigDecimal(columns[3]));
        setHighestPrice(new BigDecimal(columns[4]));
        setMinimumPrice(new BigDecimal(columns[5]));
        setCompeteBuyPrice(new BigDecimal(columns[6]));
        setCompeteSellPrice(new BigDecimal(columns[7]));
        setTransactionNum(Long.valueOf(columns[8]));
        setTransactionAmount(new BigDecimal(columns[9]));
    }

    /***
     * 总股本
     */
    private  BigDecimal equityTotal;
    /***
     * 流通股本
     */
    private  BigDecimal equityCirculate;


    public BigDecimal getEquityTotal() {
        return equityTotal;
    }

    public void setEquityTotal(BigDecimal equityTotal) {
        this.equityTotal = equityTotal;
    }

    public BigDecimal getEquityCirculate() {
        return equityCirculate;
    }

    public void setEquityCirculate(BigDecimal equityCirculate) {
        this.equityCirculate = equityCirculate;
    }
}
