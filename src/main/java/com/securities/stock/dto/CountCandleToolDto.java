package com.securities.stock.dto;

public class CountCandleToolDto {

    private String column;

    private Long stockId;

    private Integer days;

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public Long getStockId() {
        return stockId;
    }

    public void setStockId(Long stockId) {
        this.stockId = stockId;
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    public CountCandleToolDto(String column, Long stockId, Integer days) {
        this.column = column;
        this.stockId = stockId;
        this.days = days;
    }

    public CountCandleToolDto() {
    }
}
