package com.securities.stock.dto;

import java.math.BigDecimal;

public class GradeDto {
    /***
     * 等级名称
     */
    private String gradeName;
    /***
     * 申报数量
     */
    private Long applyNum;
    /***
     * 申报价格
     */
    private BigDecimal applyPrice;

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public Long getApplyNum() {
        return applyNum;
    }

    public void setApplyNum(Long applyNum) {
        this.applyNum = applyNum;
    }

    public BigDecimal getApplyPrice() {
        return applyPrice;
    }

    public void setApplyPrice(BigDecimal applyPrice) {
        this.applyPrice = applyPrice;
    }

    public GradeDto(String gradeName, Long applyNum, BigDecimal applyPrice) {
        this.gradeName = gradeName;
        this.applyNum = applyNum;
        this.applyPrice = applyPrice;
    }
    public GradeDto(){}
}
