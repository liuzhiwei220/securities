package com.securities.stock.pojo;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Table(name = "candle_${stockId}")
public class Candle extends BasePojo{

    /**
     * 主键
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    private Long id;
    /***
     * 股票id
     */
    private Long stockId;
    /***
     * 股票code
     */
    private String stockCode;
    /***
     * 开盘价
     */
    private BigDecimal openingPrice;
    /***
     * 昨天收盘价
     */
    private BigDecimal yesterdayEndPrice;
    /***
     * 当前价格
     */
    private BigDecimal currentPrice;
    /***
     * 今日最高价
     */
    private BigDecimal highestPrice;
    /***
     * 今日最低价
     */
    private BigDecimal minimumPrice;
    /***
     * 竞买价
     */
    private BigDecimal competeBuyPrice;
    /***
     * 竞卖价
     */
    private BigDecimal competeSellPrice;
    /***
     * 成交的股票数
     */
    private Long transactionNum;
    /***
     * 成交金额
     */
    private  BigDecimal transactionAmount;
    /***
     * 最近五档的交易json
     */
    private String transactionGradeJson;
    /***
     * 交易日期
     */
    private String transactionDate;
    /***
     * 交易时间
     */
    private String transactionTime;
    /***
     * 交易日的序号 时间格式的数字
     */
    private Integer daySeq;
    /***
     * 结果返回的数据
     */
    private String resultData;
    /***
     * 唯一标识,为了防止重复添加
     */
    private String onlyIdentification;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStockId() {
        return stockId;
    }

    public void setStockId(Long stockId) {
        this.stockId = stockId;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public BigDecimal getOpeningPrice() {
        return openingPrice;
    }

    public void setOpeningPrice(BigDecimal openingPrice) {
        this.openingPrice = openingPrice;
    }

    public BigDecimal getYesterdayEndPrice() {
        return yesterdayEndPrice;
    }

    public void setYesterdayEndPrice(BigDecimal yesterdayEndPrice) {
        this.yesterdayEndPrice = yesterdayEndPrice;
    }

    public BigDecimal getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(BigDecimal currentPrice) {
        this.currentPrice = currentPrice;
    }

    public BigDecimal getHighestPrice() {
        return highestPrice;
    }

    public void setHighestPrice(BigDecimal highestPrice) {
        this.highestPrice = highestPrice;
    }

    public BigDecimal getMinimumPrice() {
        return minimumPrice;
    }

    public void setMinimumPrice(BigDecimal minimumPrice) {
        this.minimumPrice = minimumPrice;
    }

    public BigDecimal getCompeteBuyPrice() {
        return competeBuyPrice;
    }

    public void setCompeteBuyPrice(BigDecimal competeBuyPrice) {
        this.competeBuyPrice = competeBuyPrice;
    }

    public BigDecimal getCompeteSellPrice() {
        return competeSellPrice;
    }

    public void setCompeteSellPrice(BigDecimal competeSellPrice) {
        this.competeSellPrice = competeSellPrice;
    }

    public Long getTransactionNum() {
        return transactionNum;
    }

    public void setTransactionNum(Long transactionNum) {
        this.transactionNum = transactionNum;
    }

    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(BigDecimal transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getTransactionGradeJson() {
        return transactionGradeJson;
    }

    public void setTransactionGradeJson(String transactionGradeJson) {
        this.transactionGradeJson = transactionGradeJson;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(String transactionTime) {
        this.transactionTime = transactionTime;
    }

    public String getResultData() {
        return resultData;
    }

    public void setResultData(String resultData) {
        this.resultData = resultData;
    }

    public String getOnlyIdentification() {
        return onlyIdentification;
    }

    public void setOnlyIdentification(String onlyIdentification) {
        this.onlyIdentification = onlyIdentification;
    }

    public Integer getDaySeq() {
        return daySeq;
    }

    public void setDaySeq(Integer daySeq) {
        this.daySeq = daySeq;
    }
}
