package com.securities.stock.pojo;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;

public class Stock extends BasePojo {

    /**
     * 主键
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    private Long id;
    /***
     * 名称
     */
    private String name;
    /***
     * 对应的代码
     */
    private String code;
    /***
     * 所属市场
     */
    private String station;
    /***
     * 板块code
     */
    private String plate;
    /***
     * 指数还是个股
     */
    private Integer type;
    /***
     * 总股本
     */
    private  BigDecimal equityTotal;
    /***
     * 流通股本
     */
    private  BigDecimal equityCirculate;

    private Integer isValid;

    private Integer synStatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public Integer getIsValid() {
        return isValid;
    }

    public void setIsValid(Integer isValid) {
        this.isValid = isValid;
    }

    public BigDecimal getEquityTotal() {
        return equityTotal;
    }

    public void setEquityTotal(BigDecimal equityTotal) {
        this.equityTotal = equityTotal;
    }

    public BigDecimal getEquityCirculate() {
        return equityCirculate;
    }

    public void setEquityCirculate(BigDecimal equityCirculate) {
        this.equityCirculate = equityCirculate;
    }

    public Integer getSynStatus() {
        return synStatus;
    }

    public void setSynStatus(Integer synStatus) {
        this.synStatus = synStatus;
    }
}
