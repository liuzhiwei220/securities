package com.securities.stock.pojo;

import java.util.Date;

public class BasePojo {
	
	/**
	 * 最后更新时间
	 */
	private Date updated;
	/**
	 * 创建时间
	 */
	private Date created;
	
	public Date getUpdated() {
		return updated;
	}
	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	
	

}
