package com.securities.stock.pojo;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;

/****
 *
 */
public class CandleView extends BasePojo {

    /**
     * 主键
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    private Long id;
    private Long stockId;
    private Long candleId;
    /****
     * 涨停价
     */
    private BigDecimal riseFinish;
    /****
     * 跌停价
     */
    private BigDecimal fallFinish;
    /***
     * 涨幅
     */
    private BigDecimal riseRange;
    /***
     * 涨跌
     */
    private BigDecimal riseUp;
    /***
     * 涨速
     */
    private BigDecimal riseVelocity;
    /****
     * 换手率
     */
    private BigDecimal exchangeRate;
    /***
     * 量比
     */
    private BigDecimal numRate;
    /***
     * 振幅
     */
    private BigDecimal amplitude;
    /***
     * 当日均价
     */
    private BigDecimal averagePrice;
    /***
     * 5日均线点
     */
    private BigDecimal ma5;
    /****
     * 10日均线点
     */
    private BigDecimal ma10;
    /****
     * 20日均线点
     */
    private BigDecimal ma20;
    /****
     * 30日均线点
     */
    private BigDecimal ma30;
    /****
     * 60日均线点
     */
    private BigDecimal ma60;
    /****
     * 120日均线点
     */
    private BigDecimal ma120;
    /***
     * 多根均线的点
     */
    private String maJson;

    /***
     * 5日交易量均线点
     */
    private BigDecimal transactionMa5;
    /***
     * 10日交易量均线点
     */
    private BigDecimal transactionMa10;
    /***
     * 交易量均线点json
     */
    private String transactionMaJson;
    /***
     * 交易日的序号 时间格式的数字
     */
    private Integer daySeq;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStockId() {
        return stockId;
    }

    public void setStockId(Long stockId) {
        this.stockId = stockId;
    }

    public Long getCandleId() {
        return candleId;
    }

    public void setCandleId(Long candleId) {
        this.candleId = candleId;
    }

    public BigDecimal getRiseFinish() {
        return riseFinish;
    }

    public void setRiseFinish(BigDecimal riseFinish) {
        this.riseFinish = riseFinish;
    }

    public BigDecimal getFallFinish() {
        return fallFinish;
    }

    public void setFallFinish(BigDecimal fallFinish) {
        this.fallFinish = fallFinish;
    }

    public BigDecimal getRiseRange() {
        return riseRange;
    }

    public void setRiseRange(BigDecimal riseRange) {
        this.riseRange = riseRange;
    }

    public BigDecimal getRiseUp() {
        return riseUp;
    }

    public void setRiseUp(BigDecimal riseUp) {
        this.riseUp = riseUp;
    }

    public BigDecimal getRiseVelocity() {
        return riseVelocity;
    }

    public void setRiseVelocity(BigDecimal riseVelocity) {
        this.riseVelocity = riseVelocity;
    }

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public BigDecimal getNumRate() {
        return numRate;
    }

    public void setNumRate(BigDecimal numRate) {
        this.numRate = numRate;
    }

    public BigDecimal getAmplitude() {
        return amplitude;
    }

    public void setAmplitude(BigDecimal amplitude) {
        this.amplitude = amplitude;
    }

    public BigDecimal getAveragePrice() {
        return averagePrice;
    }

    public void setAveragePrice(BigDecimal averagePrice) {
        this.averagePrice = averagePrice;
    }

    public BigDecimal getMa5() {
        return ma5;
    }

    public void setMa5(BigDecimal ma5) {
        this.ma5 = ma5;
    }

    public BigDecimal getMa10() {
        return ma10;
    }

    public void setMa10(BigDecimal ma10) {
        this.ma10 = ma10;
    }

    public BigDecimal getMa20() {
        return ma20;
    }

    public void setMa20(BigDecimal ma20) {
        this.ma20 = ma20;
    }

    public BigDecimal getMa30() {
        return ma30;
    }

    public void setMa30(BigDecimal ma30) {
        this.ma30 = ma30;
    }

    public BigDecimal getMa60() {
        return ma60;
    }

    public void setMa60(BigDecimal ma60) {
        this.ma60 = ma60;
    }

    public BigDecimal getMa120() {
        return ma120;
    }

    public void setMa120(BigDecimal ma120) {
        this.ma120 = ma120;
    }

    public String getMaJson() {
        return maJson;
    }

    public void setMaJson(String maJson) {
        this.maJson = maJson;
    }

    public BigDecimal getTransactionMa5() {
        return transactionMa5;
    }

    public void setTransactionMa5(BigDecimal transactionMa5) {
        this.transactionMa5 = transactionMa5;
    }

    public BigDecimal getTransactionMa10() {
        return transactionMa10;
    }

    public void setTransactionMa10(BigDecimal transactionMa10) {
        this.transactionMa10 = transactionMa10;
    }

    public String getTransactionMaJson() {
        return transactionMaJson;
    }

    public void setTransactionMaJson(String transactionMaJson) {
        this.transactionMaJson = transactionMaJson;
    }

    public Integer getDaySeq() {
        return daySeq;
    }

    public void setDaySeq(Integer daySeq) {
        this.daySeq = daySeq;
    }
}
