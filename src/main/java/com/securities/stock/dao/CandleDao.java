package com.securities.stock.dao;

import com.securities.stock.dto.CandleDto;
import com.securities.stock.mapper.CandleMapper;
import com.securities.stock.mapper.CandleViewMapper;
import com.securities.stock.pojo.Candle;
import com.securities.stock.pojo.CandleView;
import com.securities.stock.utils.Md5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

@Service
public class CandleDao {

    @Autowired
    private CandleMapper candleMapper;

    @Autowired
    private CandleViewMapper candleViewMapper;

    private  static final BigDecimal  PROPORTION_ATOM=new BigDecimal(100);

    private  static  final  int DIVIDE_SCALE=8;

    public void addCandle(CandleDto candle) {
        String onlyIdentification= Md5Util.encode(candle.getResultData());
        candle.setOnlyIdentification(onlyIdentification);
        Candle candleCount=new Candle();
        candleCount.setStockId(candle.getStockId());
        candleCount.setOnlyIdentification(candle.getOnlyIdentification());
        if(candleMapper.selectCount(candleCount)==0){
            candle.setCreated(new Date());
            candle.setUpdated(candle.getCreated());
            candleMapper.insert(candle);
            CandleView candleView= toView(candle);
            candleViewMapper.insert(candleView);
        }
    }

    private CandleView toView(CandleDto candle){
        CandleView view=new CandleView();
        view.setStockId(candle.getStockId());
        view.setCandleId(candle.getId());
        view.setDaySeq(candle.getDaySeq());
        //涨停价格
        view.setRiseFinish(candle.getYesterdayEndPrice().multiply(new BigDecimal(1.1)));
        //跌停价格
        view.setFallFinish(candle.getYesterdayEndPrice().multiply(new BigDecimal(0.9)));
        if(BigDecimal.ZERO.compareTo(candle.getYesterdayEndPrice())!=0) {
            //振幅:(当日最高价-当日最低价)/昨日收盘价
            BigDecimal amplitude = candle.getHighestPrice().subtract(candle.getMinimumPrice()).multiply(PROPORTION_ATOM).divide(candle.getYesterdayEndPrice(), DIVIDE_SCALE,
                    RoundingMode.UP);
            view.setAmplitude(amplitude);
            //涨幅:(当日收盘价-昨日收盘价)/昨日收盘价
            BigDecimal riseRange=candle.getCurrentPrice().subtract(candle.getYesterdayEndPrice()).multiply(PROPORTION_ATOM).divide(candle.getYesterdayEndPrice(),DIVIDE_SCALE,
                    RoundingMode.UP);
            view.setRiseRange(riseRange);
        }
        //换手率
        if(BigDecimal.ZERO.compareTo(candle.getEquityCirculate())!=0){
            BigDecimal exchangeRate=new BigDecimal(candle.getTransactionNum()).multiply(PROPORTION_ATOM).divide(candle.getEquityCirculate(),DIVIDE_SCALE,
                    RoundingMode.UP);
            view.setExchangeRate(exchangeRate);
        }
        //涨跌:当日收盘价-昨日收盘价
        BigDecimal riseUp=candle.getCurrentPrice().subtract(candle.getYesterdayEndPrice());
        view.setRiseUp(riseUp);
        //涨速 可以理解为股价上涨的加速度
        BigDecimal riseVelocity=riseUp.divide(new BigDecimal(240),DIVIDE_SCALE,
                RoundingMode.UP);
        //view.setRiseVelocity(riseVelocity);
        //当日均价
        if (candle.getTransactionNum() > 0) {
            BigDecimal averagePrice=candle.getTransactionAmount().divide(new BigDecimal(candle.getTransactionNum()),DIVIDE_SCALE,
                    RoundingMode.UP);
            view.setAveragePrice(averagePrice);
        }
        return view;
    }


    public List<Candle> selectAll( Long stockId){
        return candleMapper.selectAll(stockId);
    }
}
