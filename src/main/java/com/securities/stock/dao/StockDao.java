package com.securities.stock.dao;

import com.github.pagehelper.PageHelper;
import com.securities.stock.mapper.StockMapper;
import com.securities.stock.pojo.Stock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class StockDao {

	@Autowired
	private StockMapper stockMapper;


	public  List<Stock> selectAll(){
		return 	stockMapper.select(new Stock());

	}

	/***
	 * 根据条件获取一条记录,如果没有满足添加的返回null
	 * @param param
	 * @return
	 */
	public Stock selectOneByInfo(Stock param){
		PageHelper.startPage(1,1);
		List<Stock> list=stockMapper.select(param);
		if(CollectionUtils.isEmpty(list)){
			return null;
		}
		return list.get(0);

	}

	public  List<Stock> selectByStation(String station){
		Stock stock=new Stock();
		stock.setStation(station);
		stock.setIsValid(1);
		return 	stockMapper.select(stock);

	}

	public void saveBatch(List<Stock> stocks) {
		stockMapper.saveBatch(stocks);

	}

	public  void updateByPrimaryKeySelective(Stock stock){
		stockMapper.updateByPrimaryKeySelective(stock);
	}

	public void resetAllSynStatus() {
		stockMapper.resetAllSynStatus();
	}
}
