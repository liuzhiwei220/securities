package com.securities.stock.job;

import com.securities.stock.crawler.CrawlerService;
import com.securities.stock.dao.StockDao;
import com.securities.stock.pojo.Stock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CrawlerJob {


    private final static Logger LOGGER = LoggerFactory.getLogger(CrawlerJob.class);


    @Autowired
    private  CrawlerService crawlerService;

    @Autowired
    private StockDao stockDao;

    public  void  crawlSh()throws Exception{
        List<Stock> stocks= crawlerService.crawlerSh();
        stockDao.saveBatch(stocks);
    }

    public  void  crawlSz()throws Exception{
        List<Stock> stocks= crawlerService.crawlerSz();
        stockDao.saveBatch(stocks);
    }
    public  void  crawlCyb()throws Exception{
        List<Stock> stocks= crawlerService.crawlerCyb();
        stockDao.saveBatch(stocks);
    }

    public  void  crawlStcok()throws Exception{
        List<Stock> stocks=new ArrayList<>();
        List<Stock> stockSh= crawlerService.crawlerSh();
        List<Stock> stockSz= crawlerService.crawlerCyb();
        List<Stock> stocCyb= crawlerService.crawlerSz();
        stocks.addAll(stockSh);
        stocks.addAll(stockSz);
        stocks.addAll(stocCyb);
        stockDao.saveBatch(stocks);
    }

    /***
     * 根据总股本和流通股的最新信息
     * @throws Exception
     */

    public void synEquity()throws Exception{
        Stock param=new Stock();
        param.setSynStatus(-1);
        param.setType(1);
        Stock s= stockDao.selectOneByInfo(param);
        if(s==null){
            LOGGER.info("没有满足条件的对象");
            return;
        }
        Stock gb= crawlerService.crawlerEquity(s);
        gb.setSynStatus(1);
        gb.setUpdated(new Date());
        stockDao.updateByPrimaryKeySelective(gb);
    }

    public void resetSynStatus(){
        stockDao.resetAllSynStatus();
    }

}
