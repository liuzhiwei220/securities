package com.securities.stock.job;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.securities.stock.dao.CandleDao;
import com.securities.stock.dao.StockDao;
import com.securities.stock.dto.CandleDto;
import com.securities.stock.dto.GradeDto;
import com.securities.stock.enums.StationEnum;
import com.securities.stock.pojo.Candle;
import com.securities.stock.pojo.Stock;
import com.securities.stock.utils.Md5Util;
import com.securities.stock.utils.SSLHttpClientUtil;
import org.apache.commons.collections.ArrayStack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CandleJob {


    private final static Logger LOGGER = LoggerFactory.getLogger(CandleJob.class);

    @Autowired
    private StockDao stockDao;

    @Autowired
    private CandleDao candleDao;

    /***
     * 上海数据的抓取
     * @throws Exception
     */
    public  void  crawlStockSh()throws Exception{
        crawlStock(StationEnum.SH);
    }

    /***
     * 深圳数据的抓取
     * @throws Exception
     */
    public  void  crawlStockSz()throws Exception{
        crawlStock(StationEnum.SZ);
    }

    /***
     * 创业板数据的抓取
     * @throws Exception
     */
    public  void  crawlStockCyb()throws Exception{
        crawlStock(StationEnum.CYB);
    }


    private   void  crawlStock(StationEnum s)throws Exception{
        List<Stock> stocks= stockDao.selectByStation(s.getCode());
        if (stocks!=null&&stocks.size()>0){
            for(Stock stock:stocks){
                String crawlUrl="http://hq.sinajs.cn/list="+stock.getStation()+stock.getCode();
                LOGGER.info("执行get 请求,{}",crawlUrl);
                String crawData= SSLHttpClientUtil.get(crawlUrl);
                LOGGER.info("返回结果为:{}",crawData);
                if(crawData.length()>0) {
                    int i = crawData.indexOf("\"");
                    if (i == 0) {
                        continue;
                    }
                    String stockData = crawData.substring(i + 1, crawData.length() - 4);
                    String[] stockColumns = stockData.split(",");
                    CandleDto candle = new CandleDto(stockColumns);
                    try {
                        candle.setEquityCirculate(stock.getEquityCirculate());
                        candle.setEquityTotal(stock.getEquityTotal());
                        candle.setStockId(stock.getId());
                        candle.setStockCode(stock.getCode());
                        List<GradeDto> grades = new ArrayList<>();
                        GradeDto buy1 = new GradeDto("买一", Long.valueOf(stockColumns[10]), new BigDecimal(stockColumns[11]));
                        GradeDto buy2 = new GradeDto("买二", Long.valueOf(stockColumns[12]), new BigDecimal(stockColumns[13]));
                        GradeDto buy3 = new GradeDto("买三", Long.valueOf(stockColumns[14]), new BigDecimal(stockColumns[15]));
                        GradeDto buy4 = new GradeDto("买四", Long.valueOf(stockColumns[16]), new BigDecimal(stockColumns[17]));
                        GradeDto buy5 = new GradeDto("买五", Long.valueOf(stockColumns[18]), new BigDecimal(stockColumns[19]));
                        grades.add(buy1);
                        grades.add(buy2);
                        grades.add(buy3);
                        grades.add(buy4);
                        grades.add(buy5);
                        GradeDto sell1 = new GradeDto("卖一", Long.valueOf(stockColumns[20]), new BigDecimal(stockColumns[21]));
                        GradeDto sell2 = new GradeDto("卖二", Long.valueOf(stockColumns[22]), new BigDecimal(stockColumns[23]));
                        GradeDto sell3 = new GradeDto("卖三", Long.valueOf(stockColumns[24]), new BigDecimal(stockColumns[25]));
                        GradeDto sell4 = new GradeDto("卖四", Long.valueOf(stockColumns[26]), new BigDecimal(stockColumns[27]));
                        GradeDto sell5 = new GradeDto("卖五", Long.valueOf(stockColumns[28]), new BigDecimal(stockColumns[29]));
                        grades.add(sell1);
                        grades.add(sell2);
                        grades.add(sell3);
                        grades.add(sell4);
                        grades.add(sell5);
                        candle.setTransactionGradeJson(JSON.toJSONString(grades));
                        candle.setTransactionDate(stockColumns[30]);
                        candle.setTransactionTime(stockColumns[31]);
                        candle.setResultData(crawData);
                        candle.setDaySeq(Integer.valueOf(candle.getTransactionDate().replace("-", "")));
                        candleDao.addCandle(candle);
                    } catch (Exception e) {
                        LOGGER.info("{}添加失败",candle.getStockCode());
                        e.printStackTrace();
                    }
                }
            }
        }
    }





}
