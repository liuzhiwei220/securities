package com.securities.stock.job;

import com.securities.stock.dao.CandleViewDao;
import com.securities.stock.pojo.CandleView;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class CandleViewJob {


    @Autowired
    private CandleViewDao candleViewDao;

    public void analysisCandleView0(){
        Long stockId=10L;
        List<CandleView> views= candleViewDao.queryAnalysisByStockId(stockId);
        for(CandleView view:views){
            candleViewDao.analysisCandleView(view);
        }
    }
    public void analysisCandleView1(){
        Long stockId=10L+1;
        List<CandleView> views= candleViewDao.queryAnalysisByStockId(stockId);
        for(CandleView view:views){
            candleViewDao.analysisCandleView(view);
        }

    }
    public void analysisCandleView2(){
        Long stockId=10L+2;
        List<CandleView> views= candleViewDao.queryAnalysisByStockId(stockId);
        for(CandleView view:views){
            candleViewDao.analysisCandleView(view);
        }
    }
    public void analysisCandleView3(){
        Long stockId=10L+3;
        List<CandleView> views= candleViewDao.queryAnalysisByStockId(stockId);
        for(CandleView view:views){
            candleViewDao.analysisCandleView(view);
        }
    }
    public void analysisCandleView4(){
        Long stockId=10L+4;
        List<CandleView> views= candleViewDao.queryAnalysisByStockId(stockId);
        for(CandleView view:views){
            candleViewDao.analysisCandleView(view);
        }
    }
    public void analysisCandleView5(){
        Long stockId=10L+5;
        List<CandleView> views= candleViewDao.queryAnalysisByStockId(stockId);
        for(CandleView view:views){
            candleViewDao.analysisCandleView(view);
        }
    }

    public void analysisCandleView6(){
        Long stockId=10L+6;
        List<CandleView> views= candleViewDao.queryAnalysisByStockId(stockId);
        for(CandleView view:views){
            candleViewDao.analysisCandleView(view);
        }
    }

    public void analysisCandleView7(){
        Long stockId=10L+7;
        List<CandleView> views= candleViewDao.queryAnalysisByStockId(stockId);
        for(CandleView view:views){
            candleViewDao.analysisCandleView(view);
        }
    }
    public void analysisCandleView8(){
        Long stockId=10L+8;
        List<CandleView> views= candleViewDao.queryAnalysisByStockId(stockId);
        for(CandleView view:views){
            candleViewDao.analysisCandleView(view);
        }
    }
    public void analysisCandleView9(){
        Long stockId=10L+9;
        List<CandleView> views= candleViewDao.queryAnalysisByStockId(stockId);
        for(CandleView view:views){
            candleViewDao.analysisCandleView(view);
        }
    }
}
