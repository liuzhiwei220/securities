package com.securities.stock.mapper;

import com.securities.stock.dto.CountCandleToolDto;
import com.securities.stock.pojo.Candle;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface CandleMapper  {

    int insert(Candle candle);

    Long selectCount(Candle candle);

    List<Candle> selectAll(@Param("stockId") Long stockId);

    List<Candle> queryClosingPrice(@Param("stockId") Long stockId);

    List<Candle> queryTransactionNum(@Param("stockId") Long stockId);

    List<BigDecimal> queryColumnByDays(CountCandleToolDto countCandleToolDto);



}
