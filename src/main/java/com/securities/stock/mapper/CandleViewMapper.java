package com.securities.stock.mapper;

import com.securities.stock.pojo.CandleView;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CandleViewMapper  {


    List<CandleView> selectAnalysisByStockId(@Param("stockId") Long stockId);

    int insert(CandleView view);

    int updateByPrimaryKeySelective(CandleView view);
}
