package com.securities.stock.mapper.plugin;

import static org.apache.ibatis.jdbc.SqlBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SqlBuilder.DELETE_FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.scripting.xmltags.ForEachSqlNode;
import org.apache.ibatis.scripting.xmltags.MixedSqlNode;
import org.apache.ibatis.scripting.xmltags.SetSqlNode;
import org.apache.ibatis.scripting.xmltags.SqlNode;
import org.apache.ibatis.scripting.xmltags.StaticTextSqlNode;
import org.apache.ibatis.scripting.xmltags.WhereSqlNode;

import com.github.abel533.mapper.MapperProvider;
import com.github.abel533.mapperhelper.EntityHelper;
import com.github.abel533.mapperhelper.MapperHelper;
/**
 * 改写默认的MapperProvider，添加根据id批量删除方法，支持动态字段的查询
 *
 */
public class SecuritiesMapperProvider extends MapperProvider {

	public SecuritiesMapperProvider(Class<?> mapperClass, MapperHelper mapperHelper) {
		super(mapperClass, mapperHelper);
	}

	/**
	 * 通过主键更新不为null的字段
	 *
	 * @param ms
	 * @return
	 */
	@Override
	public SqlNode updateByPrimaryKeySelective(MappedStatement ms) {
		Class<?> entityClass = getSelectReturnType(ms);
		List<SqlNode> sqlNodes = new ArrayList<SqlNode>();
		//update table
		sqlNodes.add(new StaticTextSqlNode("UPDATE " + tableName(entityClass)));
		//获取全部列
		Set<EntityHelper.EntityColumn> columnList = EntityHelper.getColumns(entityClass);
		List<SqlNode> ifNodes = new ArrayList<SqlNode>();
		//全部的if property!=null and property!=''
		for (EntityHelper.EntityColumn column : columnList) {
			if(!column.isId()){
				StaticTextSqlNode columnNode = new StaticTextSqlNode(column.getColumn() + " = #{" + column.getProperty() + "}, ");
				ifNodes.add(getIfNotNull(column, columnNode));
			}
		}
		sqlNodes.add(new SetSqlNode(ms.getConfiguration(), new MixedSqlNode(ifNodes)));
		//获取全部的主键的列
		columnList = EntityHelper.getPKColumns(entityClass);
		List<SqlNode> whereNodes = new ArrayList<SqlNode>();
		boolean first = true;
		//where 主键=#{property} 条件
		for (EntityHelper.EntityColumn column : columnList) {
			whereNodes.add(getColumnEqualsProperty(column, first));
			first = false;
		}
		sqlNodes.add(new WhereSqlNode(ms.getConfiguration(), new MixedSqlNode(whereNodes)));
		return new MixedSqlNode(sqlNodes);
	}
	/**
	 * 通过ids批量删除
	 *
	 * @param ms
	 * @return
	 */
	public SqlNode deleteByIDS(MappedStatement ms) {
		Class<?> entityClass = getSelectReturnType(ms);
		Set<EntityHelper.EntityColumn> entityColumns = EntityHelper.getPKColumns(entityClass);
		EntityHelper.EntityColumn entityColumn = null;
		for (EntityHelper.EntityColumn entity : entityColumns) {
			entityColumn = entity;
			break;
		}
		EntityHelper.EntityColumn column = entityColumn;
		List<SqlNode> sqlNodes = new ArrayList<SqlNode>();
		// 开始拼sql
		BEGIN();
		// delete from table
		DELETE_FROM(tableName(entityClass));
		// 得到sql
		String sql = SQL();
		// 静态SQL部分
		sqlNodes.add(new StaticTextSqlNode(sql + " WHERE " + column.getColumn() + " IN "));
		// 构造foreach sql
		SqlNode foreach = new ForEachSqlNode(ms.getConfiguration(), new StaticTextSqlNode("#{"
				+ column.getProperty() + "}"), "ids", "index", column.getProperty(), "(", ")", ",");
		sqlNodes.add(foreach);
		return new MixedSqlNode(sqlNodes);
	}
}
