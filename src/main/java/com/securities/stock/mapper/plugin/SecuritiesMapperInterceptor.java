package com.securities.stock.mapper.plugin;

import java.util.Properties;

import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.abel533.mapperhelper.MapperInterceptor;

@Intercepts({
    @Signature(type = Executor.class, method = "query", args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class}),
    @Signature(type = Executor.class, method = "update", args = {MappedStatement.class, Object.class})
})
public class SecuritiesMapperInterceptor extends MapperInterceptor {

	

	private static final Logger LOGGER = LoggerFactory.getLogger(SecuritiesMapperInterceptor.class);
	@Override
	public void setProperties(Properties properties) {
		properties.setProperty("notEmpty", "true");
		LOGGER.debug("自定义mybaits拦截器属性设置notEmpty:{}",properties.getProperty("notEmpty"));
		super.setProperties(properties);
	}
	
	

}
