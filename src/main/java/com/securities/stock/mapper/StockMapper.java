package com.securities.stock.mapper;

import com.securities.stock.mapper.plugin.SecuritiesMapper;
import com.securities.stock.pojo.Stock;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface StockMapper extends SecuritiesMapper<Stock> {

    void saveBatch(@Param("stocks") List<Stock> stocks);

    void resetAllSynStatus();
}
