
package com.securities.stock.main;

import com.securities.stock.utils.SSLHttpClientUtil;
import com.securities.stock.utils.service.redis.RedisService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.net.InetAddress;


public class Main {

	private final static Logger LOGGER = LoggerFactory.getLogger(Main.class);
	private final static int START_TIME=60*5;

	public static void main(String[] args) throws Exception {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:META-INF/spring/*.xml");
		context.start();
		RedisService redisService=	context.getBean(RedisService.class);
		InetAddress addr = InetAddress.getLocalHost();
		redisService.set("start_"+addr.getHostAddress()+"_"+addr.getHostName(), addr.getHostAddress()+"启动成功;主机名为:"+addr.getHostName(),START_TIME);
		LOGGER.info(addr.getHostName()+"启动成功,测试redis成功");
		Thread.sleep(2000);
		System.out.println("服务正在监听，按任意键退出");
		System.in.read();
		/*String rs= SSLHttpClientUtil.get("http://hq.sinajs.cn/list=sh885531");
		int i=rs.indexOf("\"");
		LOGGER.info(rs.substring(i+1,rs.length()-4));*/
		//https://blog.csdn.net/fangquan1980/article/details/80006840



	}
}
