package com.securities.stock.utils.service.redis;

public interface IRedisFunction<E, T> {

    public T callBack(E e);

}
