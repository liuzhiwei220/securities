package com.securities.stock.utils;
/**
 * 时间常量
 *
 */
public interface DateTimeConstant {

	/**
	 * 时分秒换算单位
	 */
	int TIME_CONVERSION=60;
	/**
	 * 一秒对应的毫秒值
	 */
	long SECOND=1000L;

	/**
	 * 一分钟对应的毫秒
	 */
	long MINUTE=SECOND*TIME_CONVERSION;
	/**
	 * 一小时对应的毫秒
	 */
	long HOUR=MINUTE*TIME_CONVERSION;
	/**
	 * 
	 */
	int HOUR_TO_DAY=24;
	/**
	 * 一天对应的毫秒值
	 */
	long DAY=HOUR*HOUR_TO_DAY;
	/**
	 * 
	 */
	int DAY_TO_WEEK=7;
	/**
	 * 一周对应的毫秒值
	 */
	long WEEK=DAY*DAY_TO_WEEK;
	int DAY_TO_MONTH=30;
	/**
	 * 一个月对应的毫秒值
	 */
	long MONTH=DAY*DAY_TO_MONTH;

}
