package com.securities.stock.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {

	private final static SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");
	private final static SimpleDateFormat sdfYearShort = new SimpleDateFormat("yy");
	private final static SimpleDateFormat sdfMonth = new SimpleDateFormat("yyyy-MM");
	private final static SimpleDateFormat sdfMonth2 = new SimpleDateFormat("yyyyMM");
	private final static SimpleDateFormat sdfDay = new SimpleDateFormat("yyyy-MM-dd");
	private final static SimpleDateFormat sdfDays = new SimpleDateFormat("yyyyMMdd");
	private final static SimpleDateFormat sdfDays2 = new SimpleDateFormat("yyMMdd");
	private final static SimpleDateFormat sdfTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private final static SimpleDateFormat sdfMinutes = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	private final static SimpleDateFormat sdfHour = new SimpleDateFormat("HH:mm");
	private final static SimpleDateFormat sdfDigitalTime = new SimpleDateFormat("yyyyMMddHHmmss");
	private final static SimpleDateFormat sdfMD = new SimpleDateFormat("MM-dd");
	/**
	 * 获取当天还有多少毫秒结束
	 * @return
	 */
	public static Long endTimeInMillis(){
		Calendar todayEnd = Calendar.getInstance();
		// Calendar.HOUR 12小时制 HOUR_OF_DAY 24小时制
		todayEnd.set(Calendar.HOUR_OF_DAY, 23);
		todayEnd.set(Calendar.MINUTE, 59);
		todayEnd.set(Calendar.MILLISECOND, 999);
		todayEnd.set(Calendar.SECOND, 59);
		return  todayEnd.getTimeInMillis()-System.currentTimeMillis();
	}

	
	/**
	 * 获取YYYY格式
	 * @return
	 */
	public static String getYear() {
		return sdfYear.format(new Date());
	}
	/**
	 * 获取yyyyMMddHHmmss格式的时间
	 * @param minute 加减分钟数
	 * @return
	 */
	public static String getSpecifiedTime(int minute) {
		Calendar nowTime = Calendar.getInstance();
		nowTime.add(Calendar.MINUTE, minute);
		return sdfDigitalTime.format(nowTime.getTime());
	}
	/**
	 * 获取YYYY-MM-DD格式
	 * @return
	 */
	public static String getDigitalTime() {
		return sdfDigitalTime.format(new Date());
	}
	/**
	 * 获取YYYY-MM-DD格式
	 * @return
	 */
	public static String getDay() {
		return sdfDay.format(new Date());
	}

	/**
	 * 格式化月
	 * @param date
	 * @return
	 */
	public static String getMonth(Date date) {
		return sdfMonth.format(date);
	}
	
	/**
	 * 格式化月
	 * @param date 
	 * @return yyyyMM
	 */
	public static String getMonth2(Date date) {
		return sdfMonth2.format(date);
	}

	
	/**
	 * 获取YYYYMMDD格式
	 * @return
	 */
	public static String getDays(){
		return sdfDays.format(new Date());
	}
	
	/**
	 * 获取YYMMDD格式
	 * @return
	 */
	public static String getDayFt(){
		return sdfDays2.format(new Date());
	}
	
	/**
	 * 获取yy格式
	 * @return
	 */
	public static String getShortYear(){
		return sdfYearShort.format(new Date());
	}
	
	/**
	 * 获取YYYY-MM-DD HH:mm格式
	 * @return
	 */
	public static String getMinutes() {
		return sdfMinutes.format(new Date());
	}
	
	/**
	 * 获取YYYY-MM-DD HH:mm:ss格式
	 * @return
	 */
	public static String getTime() {
		return sdfTime.format(new Date());
	}
	  /**
     * Date 转字符串形式(HH:mm)
     * @describe <功能描述>
     * @param date
     * @return
     */
    public static String dateFormatHour(Date date){
    	return sdfHour.format(date);
    }
    
    
    /**
     * Date  转字符串形式(MM-dd)
     * @describe <功能描述>
     * @param date
     * @return
     */
    public static String sdfMDFormat(Date date){
    	return sdfMD.format(date);
    }
    
   
    
    /**
     * Date 转字符串形式(yyyy-MM-dd HH:mm:ss)
     * @describe <功能描述>
     * @param date
     * @return
     */
    public static String formatDateFull(Date date){
    	return sdfTime.format(date);
    }
}
