package com.securities.stock.enums;

public enum StationEnum {
    SH("sh", "上证"),
    SZ("sz", "深证"),
    CYB("cyb", "创业板"),
    ;
    private String code;

    private String name;

    StationEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }
}
