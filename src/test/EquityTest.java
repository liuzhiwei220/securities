import org.fusesource.jansi.Ansi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.fusesource.jansi.Ansi.*;
import static org.fusesource.jansi.Ansi.Color.*;
import static org.fusesource.jansi.Ansi.Color.GREEN;
import static org.fusesource.jansi.Ansi.Color.RED;
import static org.fusesource.jansi.Ansi.ansi;


public class EquityTest {


    private static final Logger LOGGER = LoggerFactory.getLogger(EquityTest.class);

    public static List<Candle>  initTrend(Candle first){
        List<Candle> trend=new ArrayList<>();
        trend.add(first);
        Candle yesterday=first;
        for(int i=2;i<=300;i++){
            Candle today=new Candle();
            today.setIndex(i);
            BigDecimal range=range();
            BigDecimal yesterdayEnd=new BigDecimal(yesterday.getEnd());
            BigDecimal begin=yesterdayEnd.multiply(range);
           // LOGGER.info("JHJJ,KPJ:[ begin={} ] = {} X {}",begin.setScale(2, RoundingMode.UP),yesterday.getEnd(),  printPercentage(range.subtract(new BigDecimal(1))));
            today.setBegin(begin.setScale(2, RoundingMode.UP).doubleValue());
            BigDecimal max=begin.setScale(8, RoundingMode.UP);
            BigDecimal competeMax=yesterdayEnd.multiply(new BigDecimal(1.1)).setScale(8, RoundingMode.UP);
            BigDecimal min=begin.setScale(8, RoundingMode.UP);
            BigDecimal competeMin=yesterdayEnd.multiply(new BigDecimal(0.9)).setScale(8, RoundingMode.UP);
            BigDecimal temp=begin.setScale(8, RoundingMode.UP);
            int compete=10;
            //模拟10次竞争
            for(int j=1;j<=compete;j++){
                BigDecimal r=range();
                temp=temp.multiply(r);
                //LOGGER.info("第{}次compete 开始 [r={},max={},min={}]",j,r,max,min);
                if(temp.doubleValue()>competeMax.doubleValue()){
                    temp=competeMax;
                }
                if(temp.doubleValue()<competeMin.doubleValue()){
                    temp=competeMin;
                }
                if(temp.doubleValue()>max.doubleValue()){
                    max=temp.setScale(8, RoundingMode.UP);
                }
                if(temp.doubleValue()<min.doubleValue()){
                    min=temp.setScale(8, RoundingMode.UP);
                }
                if(compete==j){
                    today.setEnd(temp.setScale(2, RoundingMode.UP).doubleValue());
                }
                //LOGGER.info("第{}次compete 结束 [temp={},r={},max={},min={}]",j,temp.setScale(2, RoundingMode.UP),  printPercentage(r.subtract(new BigDecimal(1))), max,min);
            }
            today.setMax(max.setScale(2, RoundingMode.UP).doubleValue());
            today.setMin(min.setScale(2, RoundingMode.UP).doubleValue());
            today.setRange(new BigDecimal((today.getEnd()-yesterdayEnd.doubleValue())/yesterdayEnd.doubleValue()));
            today.setYesterdayEnd(yesterdayEnd.doubleValue());
           // LOGGER.info("第{}天结束,最后的结果为:{}",i,today.toString());
            trend.add(today);
            yesterday=today;
        }
        return trend;
    }


    public static void main(String[] args) {
        Candle first=new Candle();
        first.setBegin(9d);
        first.setEnd(9d);
        first.setMax(9d);
        first.setMin(9d);
        first.setIndex(1);
        List<Candle> trends= initTrend(first);
        System.out.println(trends.toString());

    }

    private static  Ansi printPercentage(BigDecimal bigDecimal){
        if(bigDecimal==null){
            return null;
        }
        if(bigDecimal.compareTo(BigDecimal.ZERO)>0){
            return    ansi().eraseScreen().fg(RED).a(bigDecimal.multiply(new BigDecimal(100)).setScale(2, RoundingMode.UP).toString()+"%").reset();
        }
        else {
            return    ansi().eraseScreen().fg(GREEN).a(bigDecimal.multiply(new BigDecimal(100)).setScale(2, RoundingMode.UP).toString()+"%").reset();
        }
    }

    private  static BigDecimal range(){
        BigDecimal bg = new BigDecimal(Math.random()/10);
        Random rd=new Random();
        BigDecimal range=rd.nextBoolean()?bg.add(new BigDecimal(0.009)):bg.negate();
        return  new BigDecimal(1).add(range).setScale(6, RoundingMode.UP);
    }
    /***
     * 蜡烛
     */
    public static class  Candle{

        private Integer index;
        /***
         * 开始
         */
        private Double begin;
        /***
         * 结束
         */

        private Double end;
        /***
         * 昨天结束
         */
        private Double yesterdayEnd;
        /***
         * 最大
         */
        private Double max;
        /***
         * 最小
         */
        private Double min;
        /***
         * 百分比
         */
        private BigDecimal range;

        public Double getBegin() {
            return begin;
        }

        public void setBegin(Double begin) {
            this.begin = begin;
        }


        public Double getYesterdayEnd() {
            return yesterdayEnd;
        }

        public void setYesterdayEnd(Double yesterdayEnd) {
            this.yesterdayEnd = yesterdayEnd;
        }

        public Double getEnd() {
            return end;
        }

        public void setEnd(Double end) {
            this.end = end;
        }

        public Double getMax() {
            return max;
        }

        public void setMax(Double max) {
            this.max = max;
        }

        public Double getMin() {
            return min;
        }

        public void setMin(Double min) {
            this.min = min;
        }

        public Integer getIndex() {
            return index;
        }

        public void setIndex(Integer index) {
            this.index = index;
        }

        public BigDecimal getRange() {
            return range;
        }

        public void setRange(BigDecimal range) {
            this.range = range;
        }

        public String toString() {
            return "Candle_"+index+"{" +
                    "index=" + index +
                    ", yesterdayEnd=" + yesterdayEnd +
                    ", begin=" + begin +
                    ", end=" + end +
                    ", max=" + max +
                    ", min=" + min +
                    ", range=" + printPercentage(range) +
                    '}'+"\n";
        }


    }
}
